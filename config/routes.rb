# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get 'polls', to: 'polls#index'
get 'polls/new', to: 'polls#new'
post 'polls', to: 'polls#create'
post 'polls/:id/vote', to: 'polls#vote'
