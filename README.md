# Redmine polls plugin 

Плагин голосования добавляет вкладку голосования для проектов.

### Установка

1. Из корня проекта Redmine перейдите в директорию plugins
```
cd plugins
```
2. Клонируйте репозиторий в директорию `polls`
```
git clone https://an280420@bitbucket.org/an280420/redmine-polls-plugin.git ./polls
```
3. Запустите миграцию для создания таблицы голосования

linux:
```
RAILS_ENV=production bundle exec rails redmine:plugins:migrate
```
windows:
```
bundle exec rails redmine:plugins:migrate RAILS_ENV=production
```
### Тестирование плагина

1. Для запуска тестов 

```
RAILS_ENV=test bundle exec rake test TEST=plugins/polls/test/functional/polls_controller_test.rb
```
### Удаление плагина

1. Откатите миграцию 

```
RAILS_ENV=production bundle exec rails redmine:plugins:migrate NAME=polls VERSION=0
```
2. Удалите плагин из директории `plugins/`
```
cd plugins/
rm -rf polls
```
