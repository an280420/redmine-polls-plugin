require File.expand_path('../../test_helper', __FILE__)

class PollsControllerTest < ActionController::TestCase
  fixtures :projects

  def test_index
    Project.find(1).enabled_module_names = [:polls]
    @request.session[:user_id] = 1
    get :index, params: { project_id: 1 }

    assert_response :success
    assert_template :index
  end

  def test_vote
    poll = Poll.create question: 'test poll'
    project = Project.find(1)
    project.enabled_module_names = [:polls]
    @request.session[:user_id] = 1
    post :vote, params: { project_id: project.identifier, id: poll.id, answer: 'yes'}

    assert_response :redirect
    assert_redirected_to(controller: 'polls', action: 'index', project_id: project.identifier)
  end

  def test_new
    project = Project.find(1)
    project.enabled_module_names = [:polls]
    @request.session[:user_id] = 1
    get :new, params: { project_id: project.identifier}

    assert_response :success
    assert_template :new
  end

  def test_create
    project = Project.find(1)
    project.enabled_module_names = [:polls]
    @request.session[:user_id] = 1
    post :create, params: { question: 'test_poll', project_id: project.identifier}
    poll = Poll.first
    
    assert_response :redirect
    assert_redirected_to(controller: 'polls', action: 'index', project_id: project.identifier)
  end
end
