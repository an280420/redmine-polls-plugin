class Poll < ActiveRecord::Base
  include Redmine::SafeAttributes
  safe_attributes 'question'

  def vote(answer)
    increment(answer == 'yes' ? :yes : :no)
  end
end
