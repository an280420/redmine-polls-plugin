Redmine::Plugin.register :polls do
  name 'Polls plugin'
  author 'Anton Nekrasov'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'https://bitbucket.org/an280420/redmine-polls-plugin'
  author_url 'https://github.com/an280420'

  require_dependency 'polls_hook_listener'

  # permission :polls, { polls: [:index, :vote] }, public: true
  # permission :view_polls, polls: :index
  # permission :vote_polls, polls: :vote
  project_module :polls do
    permission :view_polls, polls: :index
    permission :vote_polls, polls: :vote
    permission :new_polls, polls: :new
    permission :create_polls, polls: :create
  end
  menu :project_menu, :polls, { controller: 'polls', action: 'index' }, caption: 'Polls', after: :activity, param: :project_id

  delete_menu_item :top_menu, :my_page
  delete_menu_item :top_menu, :help
  delete_menu_item :project_menu, :activity
  delete_menu_item :project_menu, :news
end
